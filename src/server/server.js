require("dotenv").config();
var winston = require('../config/winston');
//console.log(`Your port is ${process.env.PORT}`);
//const { Pool, Client } = require('pg')
const db=require("./movies");
const database=require("./directors");
var fs = require("fs");
var data = fs.readFileSync("src/data/movies.json", "utf8");
var words = JSON.parse(data);
//console.log(words);
//pool syntax//
// const pool = new Pool()
// pool.connect()
// .then((client)=>{
//     client.query('SELECT * from movies;')
//     .then((res)=>{
//         console.log(res.rows);
//     })
//     .catch((err)=>{
//         console.log(err);
//     })
//     .finally(()=>{
//         client.release();
//     })
// })
const { Pool } = require("pg");
const pool = new Pool();
//pool.connect()
// for(let i=0;i<words.length;i++)
// {
//     pool.connect((err, client, release) => {
//         if(words[i].Metascore == "NA"){
//                  words[i].Metascore =  0
//         }
//         if(words[i].Gross_Earning_in_Mil == "NA"){
//                 words[i].Gross_Earning_in_Mil = 0
//         }
//     if (err) {
//         return console.error('Error acquiring client', err.stack)
//     }
//     client.query('INSERT INTO  movies VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)',
//         Object.values(words[i]), (err, result) => {
//         release()
//         if (err) {
//         return console.error('Error executing query', err.stack)
//         }
//         console.log("row inserted sucessfully");
//     })
//     })
// }
const express = require("express");
const morgan = require('morgan');
const app = express();  
const Logger = require('../services/logger_services')
const port = process.env.port || 3000;
const bodyParser = require("body-parser");
const logger=new Logger('app')
app.use(bodyParser.json());
app.use(morgan('combined', { stream: winston.stream }));
app.use(bodyParser.urlencoded({ extended: false }));
//directors
app.get("/api/directors",database.getdirectors)
app.get("/api/directors/:directorId",database.getdirectorsId); 
app.post("/api/directors",database.insertDirector);
app.put("/api/directors/:directorId",database.updateDirector);
app.delete("/api/directors/:directorId",database.deleteDirector);
//movies
app.get("/api/movies",db.getmovies); 
app.get("/api/movies/:movieId",db.getmoviesId);
app.post("/api/movies", db.insertNewMovies);
app.post("/api/movies/:movieId",db.updateMovieId);
app.delete("/api/movies/:movieId",db.deleteMovieId);
//using client instead of pool example//
// app.get("/api/movies",(req,res) =>{
//     client
//     .query("select movie_id, title from movies1 group by movie_id asc")
//     .then(response=>{
//         res.send(response.rows);
//     })
//     .catch(error=>{
//         console.log(error);
//     })
// })


//ERROR HANDLER
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // add this line to include winston logging
    winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
app.listen(port, () => {
    console.log(` app listening on port ${port}!`)
    logger.info(`APP LAUNCHED IN PORT ${port} `)
});