
const { Pool } = require("pg");
const pool = new Pool();

const morgan = require('morgan');
const Logger = require('../services/logger_services')
const logger=new Logger('app')

const getmovies=(req, resp) => {
    pool.connect().then(client => {
      client
        .query("select movie_id, title from movies1 group by movie_id ")
        .then(res => {
            logger.info(" displaying all the records of movies table", {
                "sucess": true
        })
          client.release();
          console.log(res.rows);
          resp.send(res.rows);
        })
        .catch(e => {
            logger.error("error in query", {
                "sucess": false
        })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
const getmoviesId= (req, resp) => {
    const id = req.params.movieId;
    pool.connect().then(client => {
      client
        .query("select movie_id, title from movies1 where movie_id=" + id)
        .then(res => {
            logger.info(" displaying movie id= "+id+ " record from the movie table", {
                "sucess": true
        })
          client.release();
          console.log(res.rows);
          resp.send(res.rows);
        })
        .catch(e => {
            logger.error(" error!! check the query", {
                "sucess": false
        })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
const insertNewMovies= (req, resp) => {
    let obj = Object.values(req.body);
    pool.connect().then(client => {
      client
        .query("insert into movies1 (movie_id, title) values($1,$2)", obj)
        .then(res => {

            const body = req.body
            let error = {}
            // Adding body of the request as log data
            logger.setLogData(body)
            logger.info("Request recieved at /api/movies", req.body)
            // We are expecting name,age and gender in the body of the request
            
            if (body.movie_id == null || body.movie_id == "") {
              logger.error("movie_id field is empty but query executed sucessfully")
              error["movie_id"] = "movie_id field is empty but query executed sucessfully"
            }
            
            if (Object.keys(error).length != 0) { 
              logger.error("Retun error response", {
              "sucess":false
            })
            resp.send("Error")
          }else{
            logger.info("Return sucess response", {
              "sucess": true
            })
            resp.send(" data inserted sucessfully")
          }

          client.release();
          console.log("INSERTED DATA sucessfully");
        })
        .catch(e => {
            logger.info("error!! check the query and the parameters", {
                "sucess": true
              })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
const deleteMovieId= (req, resp) => {
    const id = req.params.movieId;
    console.log(id);
    pool.connect().then(client => {
      client
        .query("delete from movies1 where movie_id=" + id)
        .then(res => {
            logger.info(id+" movie_id record deleted sucessfully", {
                "sucess": true
        })
          client.release();
          console.log(id + "deleted sucessfully");
          resp.send(id + "deleted sucessfully");
        })
        .catch(e => {
            logger.error(id+" movie_id record not deleted sucessfully", {
                "sucess": false
        })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
  const updateMovieId= (req, resp) => {
    const id = req.params.movieId;
    let obj = Object.values(req.body);
    pool.connect().then(client => {
      client
        .query("update movies1 set title=$1 where movie_id=" + id + ";", obj)
        .then(res => {
            const body = req.body
            let error = {}
            // Adding body of the request as log data
            logger.setLogData(body)
            logger.info("Request recieved at /api/movies/:movieId", req.body)
            
            
            if (body.title == null || body.title == "") {
              logger.error("title field is empty but query executed sucessfully")
              error["director_name"] = "title field is empty but query executed sucessfully"
            }
            
            if (Object.keys(error).length != 0) { 
              logger.error("Retun error response", {
              "sucess":false
            })
            resp.send("Error")
          }else{
            logger.info("Return sucess response", {
              "sucess": true
            })
            resp.send("data updated sucessfully");
          }
          client.release();
          console.log("data updated sucessfully");
          
        })
        .catch(e => {
            logger.error("error!! in query and check the parameters", {
                "sucess":false
              })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
module.exports={
    getmovies,
    getmoviesId,
    insertNewMovies,
    deleteMovieId,
    updateMovieId
}