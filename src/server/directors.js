const { Pool } = require("pg");
const pool = new Pool();

const morgan = require('morgan');
const Logger = require('../services/logger_services')
const logger=new Logger('app')

const getdirectors=(req, resp) => {
    pool.connect().then(client => {
      client
        .query("select director_name from directors")
        .then(res => {
                logger.info(" displaying all the records of director table", {
                    "sucess": true
            })
          client.release();
          console.log("hello from", res.rows);
          resp.send(res.rows);
          
        })
        .catch(e => {
            logger.error("error in query", {
                "sucess": false
        })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
const getdirectorsId=(req, resp) => {
    const id = req.params.directorId;
    pool.connect().then(client => {
      client
        .query("select dirid, director_name from directors where dirid=" + id)
        .then(res => {
            logger.info(" displaying director id= "+id+ " record from the director table", {
                "sucess": true
        })
          client.release();
          resp.send(res.rows);
        })
        .catch(e => {
            
                logger.error(" error!! check the query", {
                    "sucess": false
            })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
  const insertDirector=(req, resp) => {
    let obj1 = Object.values(req.body);
    pool.connect().then(client => {
      client
        .query(
          "INSERT INTO directors (dirid,Director_name) VALUES(DEFAULT,$1)",
          obj1
        )
        .then(res => {
            const body = req.body
            let error = {}
            // Adding body of the request as log data
            logger.setLogData(body)
            logger.info("Request recieved at /api/directors", req.body)
            // We are expecting name,age and gender in the body of the request
            
            if (body.director_name == null || body.director_name == "") {
              logger.error("director_name field is empty but query executed sucessfully")
              error["title"] = "director_name field is empty but query executed sucessfully"
            }
            
            if (Object.keys(error).length != 0) { 
              logger.error("Retun error response", {
              "sucess":false
            })
            resp.send("Error")
          }else{
            logger.info("Return sucess response", {
              "sucess": true
            })
            resp.send("No Error")
          }
          client.release();
          console.log("Data inserted sucessfully...", res.rows);
          //resp.send(res.rows);
        })
        .catch(e => {
            logger.error("error in the query check the parameters!!", {
                "sucess":false
              })
          client.release();
          console.error("query error", e.message, e.stack);

        });
    });
  }
  const updateDirector= (req, resp) => {
    const id = req.params.directorId;
    let obj = Object.values(req.body);
    pool.connect().then(client => {
      client
        .query(
          "update directors set director_name=$1 where dirid=" + id + ";",
          obj
        )
        .then(res => {
            const body = req.body
            let error = {}
            // Adding body of the request as log data
            logger.setLogData(body)
            logger.info("Request recieved at /api/directors/:directorId", req.body)
            
            
            if (body.director_name == null || body.director_name == "") {
              logger.error("director_name field is empty but query executed sucessfully")
              error["director_name"] = "director_name field is empty but query executed sucessfully"
            }
            
            if (Object.keys(error).length != 0) { 
              logger.error("Retun error response", {
              "sucess":false
            })
            resp.send("Error")
          }else{
            logger.info("Return sucess response", {
              "sucess": true
            })
            resp.send("No Error")
          }
          resp.send("data updated sucessfully");
          client.release();
          console.log("Data updated sucessfully sucessfully...", res.rows);
          
        })
        .catch(e => {
            logger.error("error in the query check the parameters!!", {
                "sucess":false
              })
          client.release();
          console.error("query error", e.message, e.stack);
        });
    });
  }
  const deleteDirector= (req, resp) => {
    const id = req.params.directorId;
    let obj = Object.values(req.body);
    pool.connect().then(client => {
      client
        .query("delete from directors where dirid=" + id)
        .then(res => {
            logger.info(id+" director_id deleted sucessfully", {
                "sucess": true
        })
          client.release();
          console.log("movieID:" + id + " data deleted sucessfully", res.rows);
          resp.send("movieID:" + id + " data deleted sucessfully");
        })
        .catch(e => {
            logger.error(id+" director_id not deleted sucessfully", {
                "sucess": false
        })
        logger.error("please check your query", {
            "sucess": false
    })
          client.release();
          console.error("query error", e.message, e.stack);

        });
    });
  }
module.exports={
    getdirectors,
    getdirectorsId,
    insertDirector,
    updateDirector,
    deleteDirector
}